﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using Yodo1.MAS;

public class FormulaGenerator : MonoBehaviour
{
    #region Variables
    public int complexity, lives = 5, nicelyAnswer, level =1;
    public float scoreCount, globalCount;
    public Image globalBar;
    public GameObject AllObjects;
    public Animator openDungeonAnim;

    private int a, b, c ;
    private float fillCount,  score;
    private float timer;
    private bool nextLevel,ads, x2Bool;
    
    public DragonFight dragonFight;
    public GameObject RelifePanel;
    public GameObject LeaderBoard;
    public GameObject x2;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI formulaText;
    public TextMeshProUGUI answerPrinting;
    public TextMeshProUGUI adsText;
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI flyScoreText;
    public Animator levelOne;
    public Animator flyScore;
    public Animator dragonFly;
    public AudioSource audioSource;
    public AudioClip[] audioClips;
    public Image levelBar;
    public Image adsBar;
    public Image sword;
    public Image shield;
    public Image fight;
    public Image[] hearts;
    #endregion

    void Start()
    {
        //#region ADS
        //Yodo1U3dMas.InitializeSdk();
        ////#endregion

        ////int a = 5;

        ////int align = Yodo1U3dBannerAlign.BannerBottom;


        //Yodo1U3dMas.SetInterstitialAdDelegate((Yodo1U3dAdEvent adEvent, Yodo1U3dAdError error) => {
        //    Debug.Log("[Yodo1 Mas] InterstitialAdDelegate:" + adEvent.ToString() + "\n" + error.ToString());
        //    switch (adEvent)
        //    {
        //        case Yodo1U3dAdEvent.AdClosed:
        //            Debug.Log("[Yodo1 Mas] Interstital ad has been closed.");
        //            break;
        //        case Yodo1U3dAdEvent.AdOpened:
        //            Debug.Log("[Yodo1 Mas] Interstital ad has been shown.");
        //            break;
        //        case Yodo1U3dAdEvent.AdError:
        //            Debug.Log("[Yodo1 Mas] Interstital ad error, " + error.ToString());
        //            break;
        //    }
        //});

        //bool isLoaded = Yodo1U3dMas.IsInterstitialAdLoaded();

        //Yodo1U3dMas.ShowInterstitialAd();
        //#endregion

        #region PlayerPrefs
        if (PlayerPrefs.GetInt("complexity") != 0)
            complexity = PlayerPrefs.GetInt("complexity");

        if (PlayerPrefs.GetInt("level") != 0)
            level = PlayerPrefs.GetInt("level");

        if (PlayerPrefs.GetInt("lives") != 0)
            lives = PlayerPrefs.GetInt("lives");

        if (PlayerPrefs.GetInt("sword") == 1)
            sword.color = Color.white;

        if (PlayerPrefs.GetInt("shield") == 1)
            shield.color = Color.white;

        if (PlayerPrefs.GetInt("fight") == 1)
            fight.color = Color.white;

        scoreCount = PlayerPrefs.GetFloat("score");
        globalCount = PlayerPrefs.GetFloat("globalCount");
        #endregion

        #region FirstFormula
        a = Random.Range(1, complexity);
        b = Random.Range(1, complexity);
        c = a + b;
        #endregion

        #region Texts
        levelText.text = "Level " + level.ToString();

        formulaText.text = a.ToString() + " + " + b.ToString() + " = ?";

        scoreText.text = score.ToString("0");
        #endregion

        timer = 5;

        RelifePanel.SetActive(false);

        for(int i = 0; i < lives ; i++)
        {
            hearts[i].color = Color.red;
        }
    }

    private void Update()
    {
        //Yodo1U3dMas.SetBannerAdDelegate((Yodo1U3dAdEvent adEvent, Yodo1U3dAdError error) => {
        //    Debug.Log("[Yodo1 Mas] BannerdDelegate:" + adEvent.ToString() + "\n" + error.ToString());
        //    switch (adEvent)
        //    {
        //        case Yodo1U3dAdEvent.AdClosed:
        //            Debug.Log("[Yodo1 Mas] Banner ad has been closed.");
        //            break;
        //        case Yodo1U3dAdEvent.AdOpened:
        //            Debug.Log("[Yodo1 Mas] Banner ad has been shown.");
        //            break;
        //        case Yodo1U3dAdEvent.AdError:
        //            Debug.Log("[Yodo1 Mas] Banner ad error, " + error.ToString());
        //            break;
        //    }
        //});

        //Yodo1U3dMas.ShowBannerAd();


        globalBar.fillAmount = Mathf.Lerp(globalBar.fillAmount, globalCount, Time.deltaTime * 3);

        score = Mathf.Lerp(score, scoreCount, Time.deltaTime * 4);
        PlayerPrefs.SetFloat("score", scoreCount);

        scoreText.text = score.ToString("0");

        if (nextLevel)
        {
            levelBar.fillAmount = Mathf.Lerp(levelBar.fillAmount, 0f, Time.deltaTime * 3);
        }
        else
        {
            levelBar.fillAmount = Mathf.Lerp(levelBar.fillAmount, fillCount, Time.deltaTime * 3);
        }

        if (levelBar.fillAmount >= 0.98)
        {
            level++;
            fillCount = 0;
            nextLevel = true;
            complexity += 10;
            levelText.text = "Level " + level.ToString();
            PlayerPrefs.SetInt("level", level);
            PlayerPrefs.SetInt("complexity", complexity);
            StartCoroutine(NextLevel());
        }

        if(globalBar.fillAmount > 0.172f)
        {
            PlayerPrefs.SetInt("sword", 1);
            sword.color = Color.white;
        }

        if (globalBar.fillAmount > 0.5f)
        {
            PlayerPrefs.SetInt("shield", 1);
            shield.color = Color.white;
        }

        if (globalBar.fillAmount > 0.95f)
        {
            PlayerPrefs.SetInt("fight", 1);
            fight.color = Color.white;
        }

        if(globalBar.fillAmount > 0.99)
        {
            // AllObjects.SetActive(false);
            dragonFight.enabled = true;
            dragonFly.enabled = true;
             globalCount = 0;
            openDungeonAnim.SetTrigger("open");
        }

        if (lives < 1)
        {
            RelifePanel.SetActive(true);
            timer -= Time.deltaTime;
            adsBar.fillAmount = timer / 5;
            adsText.text = timer.ToString("0");

            if(timer < 0)
            {
                if (!ads)
                {
                    level = 1;
                    lives = 5;
                    levelText.text = "Level " + level.ToString();
                    audioSource.PlayOneShot(audioClips[1]);
                    levelOne.SetTrigger("levelOne");
                    complexity = 10;
                    PlayerPrefs.DeleteAll();
                    for (int i = 0; i < 5; i++)
                    {
                        hearts[i].color = Color.red;
                    }
                }
                

                RelifePanel.SetActive(false);
                timer = 5;
            }
        }
    }

    #region Buttons
    public void Ready()
    {
        if(answerPrinting.text != "")
        {
            StartCoroutine(Computation());
        }
    }

    public void One()
    {
        answerPrinting.text += 1;
    }

    public void Two()
    {
        answerPrinting.text += 2;
    }

    public void Three()
    {
        answerPrinting.text += 3;
    }

    public void Four()
    {
        answerPrinting.text += 4;
    }

    public void Five()
    {
        answerPrinting.text += 5;
    }

    public void Six()
    {
        answerPrinting.text += 6;
    }

    public void Seven()
    {
        answerPrinting.text += 7;
    }

    public void Eight()
    {
        answerPrinting.text += 8;
    }

    public void Nine()
    {
        answerPrinting.text += 9;
    }

    public void Zero()
    {
        answerPrinting.text += 0;
    }

    public void Clear()
    {
        if (answerPrinting.text != "")
        {
            answerPrinting.text = "";
            audioSource.PlayOneShot(audioClips[1]);
        }
    }

    public void LeaderBoardButton()
    {
        LeaderBoard.SetActive(true);
    }

    public void BackLeaderBoard()
    {
        LeaderBoard.SetActive(false);
    }

    public void LookAds()
    {
        for (int i = 0; i < 5; i++)
        {
            hearts[i].color = Color.red;
        }



        lives = 5;
        timer = 5;
        audioSource.PlayOneShot(audioClips[3]);
        RelifePanel.SetActive(false);
    }
    public void RightAnswer()
    {
        globalCount = 0.99f;
    }

    public void Restart()
    {
        level = 1;
        lives = 5;
        timer = 5;
        fillCount = 0;
        scoreCount = 0;
        levelText.text = "Level " + level.ToString();
        audioSource.PlayOneShot(audioClips[5]);
        levelOne.SetTrigger("levelOne");
        complexity = 10;
        PlayerPrefs.DeleteAll();
        for (int i = 0; i < 5; i++)
        {
            hearts[i].color = Color.red;
        }
        RelifePanel.SetActive(false);
    }

    public void clickSound()
    {
        audioSource.PlayOneShot(audioClips[0]);
    }
    #endregion

    #region Coroutines
    IEnumerator Computation()
    {
        if(answerPrinting.text == "000")
        {
            globalCount = 0.99f;
        }

        if (answerPrinting.text == c.ToString())
        {
            int x2C = c * 2;

            fillCount += 0.25f;

            if(!x2Bool)
            {
                scoreCount += c;
                flyScoreText.text = "+ " + c.ToString("0");
                globalCount += 0.0050f;

            }
            else
            {
                scoreCount += x2C;
                flyScoreText.text = "+ " + x2C.ToString("0");
                globalCount += 0.01f;

            }

            nicelyAnswer++;
            flyScore.SetTrigger("fly");

            if (nicelyAnswer > 3)
            {
                x2.SetActive(true);
                x2Bool = true;
                nicelyAnswer = 0;
                if(lives < 5)
                {
                    hearts[lives].color = Color.red;
                    hearts[lives].GetComponent<Animator>().SetTrigger("Damage");
                    lives++;
                }
            }

            PlayerPrefs.SetFloat("globalCount", globalCount);
            

            formulaText.text = "that's right!";
            formulaText.color = Color.green;
            audioSource.PlayOneShot(audioClips[3]);
        }
        else
        {
            x2.SetActive (false);
            x2Bool = false;
            nicelyAnswer = 0;
            formulaText.text = "no, sorry: " + c.ToString();
            formulaText.color = Color.red;
            Damages();
        }

     
        
        answerPrinting.text = "";

        yield return new WaitForSeconds(1);

        a = Random.Range(1, complexity);
        b = Random.Range(1, complexity);
        c = a + b;

        formulaText.text = a.ToString() + " + " + b.ToString() + " = ?";
        formulaText.color = Color.white;

        yield break;
    }

    public void Damages()
    {
        lives--;
        audioSource.PlayOneShot(audioClips[2]);
        hearts[lives].color = Color.black;
        hearts[lives].GetComponent<Animator>().SetTrigger("Damage");
        PlayerPrefs.SetInt("lives", lives);
    }

    IEnumerator NextLevel()
    {
        yield return new WaitForSeconds(1);
        nextLevel = false;
        yield break;
    }
    #endregion

   
}
