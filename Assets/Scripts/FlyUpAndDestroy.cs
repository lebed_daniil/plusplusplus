﻿using System.Collections;
using UnityEngine;
using TMPro;

public class FlyUpAndDestroy : MonoBehaviour
{
    public TextMeshProUGUI damageText;

    private void Start()
    {
        StartCoroutine(Destroy());
    }

    private void Update()
    {
        damageText.alpha -= Time.deltaTime;

        transform.position += transform.up * 200 * Time.deltaTime;
    }

    IEnumerator Destroy()
    {
        yield return new WaitForSeconds(3f);
        Destroy(gameObject);

        yield break;
    }
}
