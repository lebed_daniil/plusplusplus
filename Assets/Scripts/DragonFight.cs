﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DragonFight : MonoBehaviour
{

    private float dragonHp, randomTimer;

    public Image healBar;
    public FormulaGenerator formulaGenerator;
    public GameObject damage;
    public GameObject Button;
    public AudioSource auSource;
    public AudioClip swordKick;

    private void Start()
    {
        dragonHp = 10000;
        StartCoroutine(RandomDamage());
    }

    private void Update()
    {
        healBar.fillAmount = Mathf.Lerp(healBar.fillAmount, dragonHp / 10000, Time.deltaTime * 2);
    }

    public void Click()
    {
        float randDamage = Random.Range(20, 70);

        dragonHp -= randDamage;
        auSource.PlayOneShot(swordKick);
        GameObject damages = Instantiate(damage, Button.transform.position + new Vector3(0,50,0), Quaternion.identity, transform.parent) as GameObject;
        damages.GetComponent<TextMeshProUGUI>().text = "-" + randDamage.ToString("0");

        if (dragonHp < 0)
        {
            formulaGenerator.globalCount = 0;
            formulaGenerator.scoreCount += 100000;
           // formulaGenerator.AllObjects.SetActive(true);
           formulaGenerator.openDungeonAnim.SetTrigger("close");
            this.enabled = false;
            StopAllCoroutines();
            dragonHp = 10000;
        }
    }

    IEnumerator RandomDamage()
    {
        yield return new WaitForSeconds(randomTimer);
        formulaGenerator.Damages();
        randomTimer = Random.Range(10, 20);
        yield return RandomDamage();
    }
}
